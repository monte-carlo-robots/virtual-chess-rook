"""
This main code do the analyses, the graphics, presented in my article. So, if you run the code after a day running you 
will see the graphics presented on paper.

Note that Figure 3 and 4 are the same except by an parameter, and so the Figure 8 and 9.
"""

from simulated_annealing import SimulatedAnnealing
import numpy as np
import matplotlib.pyplot as plt
from mcl import MCL


"""
Do Figure 3 and Figure 4: Performance analyses of Monte Carlo Localization in function of landmarks.

Hit rate, E_G on article, at left. Average time to first hit, F_G on article, at right.

To Figure 3 use: n = 10 (Default)
To Figure 4 use: n = 25 (Very Slow)
"""
# n = 10
#
# y_plot = []
# x_plot = []
# z_plot = []
# r = 2000
# local = MCL(n, 1, borda=False)
# for p in range(1, n * n + 1, 2):
#     mediaAcertos = 0
#     mediaPrimeiro = 0
#     for i in range(100):
#         acertos = 0
#         local.mapa = p
#         for j in range(r):
#             local.action(-2)
#             local.sensor()
#             if local.atual == local.estimado:
#                 acertos += 1
#                 if acertos == 1:
#                     mediaPrimeiro += j + 1
#         mediaAcertos += acertos / r
#
#     z_plot.append(mediaPrimeiro / 100)
#     x_plot.append(p)
#     y_plot.append(mediaAcertos / 100)
#
#
# fig = plt.figure('', figsize=(18, 6))
# plt.subplot(121)
# plt.title('Média de acertos ao longo de 2000 passos')
# plt.xlabel('Quantidade de pontos pretos')
# plt.ylabel('Média de acertos')
# plt.plot(x_plot, y_plot, color='cornflowerblue', linewidth=2, label='Média de acertos')
#
# plt.subplot(122)
# plt.title('Tempo médio para o primeiro acerto ocorrer')
# plt.xlabel('Quantidade de pontos pretos')
# plt.ylabel('Tempo médio')
# plt.plot(x_plot, z_plot, color='red', linewidth=2, label='Tempo médio')
#
# plt.legend()
# plt.show()
# fig.savefig('r2000-100vezes-n=%d.png' %n)


# Todo: Figura 5. Evolução da probabilidade dada pelo MCL para a real posição do robô ao longo do tempo, para n = 10,
#  e duas quantidades de pontos de referência, p = 1 e p = 50.


"""
Do Figure 7: Histogram of 1000 samples of E_G with r = 2000 of a fixed landmarks configuration.

The average is highlighted in red, average and standard deviation is printed.
"""


# def find_nearest(array, value):
#     array = np.asarray(array)
#     idx = (np.abs(array - value)).argmin()
#     return idx
#
#
# sim = Simu(10, mapa=50, rand=True, mini=False)
# y_plot = np.zeros(1000)
# for i in range(1000):
#     y_plot[i] = sim.analise()
# media = y_plot.sum() / len(y_plot)
#
# erro = np.add(y_plot, -y_plot.sum() / len(y_plot))
# var = np.multiply(erro, erro).sum() / len(y_plot)
# print(media)
# print(var ** 0.5)
#
# fig = plt.figure()
# plt.title('Distribuição da métrica sobre o mesmo grid')
# n, bins, patches = plt.hist(y_plot, 50, density=True, facecolor='g', alpha=0.75)
# patches[find_nearest(bins, media)].set_fc('r')
# plt.xlabel('Medida')
# plt.ylabel('Frequência da medida')
# plt.show()
# fig.savefig('analise variancia.png')
#
# """
# Do Figure 8 and Figure 9:
#
# Best hit rate, E_G on article, found in function of number of simulated annealing transitions, at left.
#
# Transitions rejections as determined by Metropolis-Hasting in function of simulated annealing transitions, at right.
#
# To Figure 8 use: rand = False
# To Figure 9 use: rand = True (default)
# """
#
#
import os
import time
os.chdir("analise_simu_met_2")
t = time.time()
os.mkdir(str(t))
os.chdir(str(t))

sim = SimulatedAnnealing(10, map_=3, rand=True, minimize=False, border=False)
rodadas = 500
x_plot = [i for i in range(rodadas)]
y_plot, z_plot, gulo_plot, ale_plot = sim.optimize(transitions=rodadas)

sim.draw_grid('ultima')
sim.mapa = sim.best
sim.draw_grid('best')

fig = plt.figure('Simulated Annealing', figsize=(18, 6))

plt.subplot(121)
plt.title('Valor máximo encontrado por rodadas')
plt.xlabel('Rodada')
plt.ylabel('Valor máximo')
plt.plot(x_plot, y_plot, color='cornflowerblue', linewidth=2, label='Valor máximo')

plt.subplot(122)
plt.title('Número de rejeição por rodadas')
plt.xlabel('Rodada')
plt.ylabel('Número de rejeição')
plt.plot(x_plot, z_plot, color='red', linewidth=2, label='Número de rejeição')

# plt.show()
fig.savefig('plot_simu_2.png')

fig = plt.figure('', figsize=(9, 9))

plt.title('Comparative performance')
plt.xlabel('Simulated Annealing turns')
plt.ylabel('Hit rate')
plt.plot(x_plot, gulo_plot, linewidth=2, label='Our method')
plt.plot(x_plot, ale_plot, linewidth=2, label='Random')

plt.legend()
plt.show()
fig.savefig('comparative.png')

# mapa = np.zeros(10 * 10, dtype=np.float64)
# mapa[:20] = np.ones(20, dtype=np.float64)
# mapa.shape = 10, 10
# sim = Simu(10, mapa=mapa, rand=True, mini=False, borda=False)
# rodadas = 500
# x_plot = [i for i in range(rodadas)]
# y_plot, z_plot = sim.new_estima(rodadas=rodadas)
#
# sim.draw_grid('ultima')
# sim.mapa = sim.best
# sim.draw_grid('best')
#
# fig = plt.figure('Simulated Annealing', figsize=(18, 6))
#
# plt.subplot(121)
# plt.title('Valor máximo encontrado por rodadas')
# plt.xlabel('Rodada')
# plt.ylabel('Valor máximo')
# plt.plot(x_plot, y_plot, color='cornflowerblue', linewidth=2, label='Valor máximo')
#
# plt.subplot(122)
# plt.title('Número de rejeição por rodadas')
# plt.xlabel('Rodada')
# plt.ylabel('Número de rejeição')
# plt.plot(x_plot, z_plot, color='red', linewidth=2, label='Número de rejeição')
#
# plt.show()
# fig.savefig('plot_simu_2.png')