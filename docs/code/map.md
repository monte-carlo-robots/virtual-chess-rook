
<a name=".map.Map"></a>
# Map

```python
class Map(object)
```

The bi-dimensional grid map implementation for Monte Carlo Localization.

Each place in the map can contain or not a landmark. Each place is an element at the `Map.matrix` attribute and
the presence of a landmark is denoted by 1 and the absence by 0. In the image representation returned by
`Map.get_image()` landmarks are denoted by black squares.

### Attributes


- `__matrix`: The map squared matrix representation.
- `__size`: The size of the map, i.e. the matrix dimension (since is a square matrix is just a number).
- `__border`: A Boolean flag indicating if the map has border or not.
- `update_cb`: A function to be called when the matrix change.

<a name=".map.Map.__init__"></a>
## \_\_init\_\_

```python
 | __init__(matrix: np.array, border: bool = False, update_cb: Callable = None)
```

Creates an instance of the Map object.

>**Arguments**:

>- `matrix`: The squared binary matrix to represent the map, 1 represents the landmark.
>- `border`: A flag indicating if the map has border or not.
>- `update_cb`: The callback function called when the matrix change. Although optional, the function
>`Map.shuffle()` and `Map.swap(e1, e2)` just work if the `Map.update_cb` is filled.

<a name=".map.Map.striped"></a>
## striped

```python
 | @classmethod
 | striped(cls, size: int, border=False)
```

Generate a Map with matrix of shape (size, size) with horizontal stripes. The first row has landmarks.

>**Arguments**:

>- `size`: The map size/dimension.
>- `border`: A flag indicating if the map has border or not.

>**Returns**:

>The generated striped Map instance.

<a name=".map.Map.random"></a>
## random

```python
 | @classmethod
 | random(cls, size: int, number_of_landmarks: int, border=False)
```

Generate a random Map with matrix of shape (size, size) with a specific number of landmarks.

>**Arguments**:

>- `size`: The map size/dimension.
>- `number_of_landmarks`: The number of landmarks.
>- `border`: A flag indicating if the map has border or not.

>**Returns**:

>A randomly generated Map instance.

<a name=".map.Map.shuffle"></a>
## shuffle

```python
 | shuffle()
```

Shuffle the Map matrix, this means randomly place the landmarks.
Also call the update callback indicating that change.

<a name=".map.Map.swap"></a>
## swap

```python
 | swap(e1, e2)
```

Swap the landmarks location. And call the update callback indicating a change in the matrix.

>**Arguments**:

>- `e1`: Coordinate of the landmark.
>- `e2`: Coordinate of the landmark.

<a name=".map.Map.border"></a>
## border

```python
 | @property
 | border()
```

>**Returns**:

>True if this Map has border, False otherwise.

<a name=".map.Map.__getitem__"></a>
## \_\_getitem\_\_

```python
 | __getitem__(item)
```

>**Returns**:

>The item at the matrix. Same as `matrix[item]`

<a name=".map.Map.matrix"></a>
## matrix

```python
 | @property
 | matrix()
```

>**Returns**:

>The matrix representation of the map. This is NOT a copy, and NEVER change it.

<a name=".map.Map.landmarks_coordinates"></a>
## landmarks\_coordinates

```python
 | @property
 | landmarks_coordinates()
```

>**Returns**:

>The coordinates of places with landmarks (black at image representation).

<a name=".map.Map.no_landmarks_coordinates"></a>
## no\_landmarks\_coordinates

```python
 | @property
 | no_landmarks_coordinates()
```

>**Returns**:

>The coordinates of places without landmarks (white at image representation).

<a name=".map.Map.size"></a>
## size

```python
 | @property
 | size() -> int
```

>**Returns**:

>The map dimension/size.

<a name=".map.Map.get_image"></a>
## get\_image

```python
 | get_image() -> Image
```

Generate the Image object of the actual map.
Black represents the landmarks (ones in the matrix) and white the absence of landmarks (zeros in the matrix).

>**Returns**:

>The generated image.

<a name=".map.Map.copy"></a>
## copy

```python
 | copy()
```

>**Returns**:

>The deepcopy of this object.

