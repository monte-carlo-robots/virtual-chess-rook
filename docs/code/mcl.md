
<a name=".mcl.MCL"></a>
# MCL

```python
class MCL(Simulator)
```

The Monte Carlo Localization algorithm implementation. Inherits the Simulator.

This implementation was not done to be general, is specific to a bi-dimensional squared grid map.

The noises are Simulator class attributes and are initialized with it default values. If you want to change the
noises values change it using the `Simulator` and not `MCL`. Also, be sure to set a new map to trigger the
pre-processing steps which uses the noise values. The default values:

 - `Simulator.p_hit = 0.9`: The probability of the measurement to be right.
 - `Simulator.p_miss = 0.1`: The probability of the measurement to be wrong.
 - `Simulator.p_exact = 0.8`: The probability of the robot perform an exact motion.
 - `Simulator.p_overshoot = 0.1`: The probability of the robot perform more than desired motion.
 - `Simulator.p_undershoot = 0.1`: The probability of the robot perform less than desired motion.

See also the `Simulator` docs for more details.

<a name=".mcl.MCL.__init__"></a>
## \_\_init\_\_

```python
 | __init__(map_: Map)
```

Construct the MCL object.
It starts an uniform probability distribution and randomly set the robot simulated position.
Also, do the pre-processing to build the sense and movement matrices.

>**Arguments**:

>- `map_`: The map required by the Monte Carlo Localization.

<a name=".mcl.MCL.move"></a>
## move

```python
 | move(cmd=-1) -> int
```

Do the movement operation at Monte Carlo Localization.
It can receive an external command (cmd) in the form:

- 0: Move left. Prefer to use `Simulator.left_cmd`.
- 1: Move right. Prefer to use `Simulator.right_cmd`.
- 2: Move up. Prefer to use `Simulator.up_cmd`.
- 3: Move down. Prefer to use `Simulator.down_cmd`.

Or if the number is negative use a navigation policy:

- -1: Random movements (default)
- -2: Minimize the probability difference between places with and without landmarks (see articles).
- -3: Minimize the expected quadratic sum. Not in the articles but also a good one (see project docs).

>**Arguments**:

>- `cmd`: The desired movement command.

>**Returns**:

>The desired movement command.

<a name=".mcl.MCL.sense"></a>
## sense

```python
 | sense(sens: bool)
```

Do the Monte Carlo Localization measurement update. Also, it normalize the probability distribution.

>**Arguments**:

>- `sens`: The sensor reading.

<a name=".mcl.MCL.estimated_position"></a>
## estimated\_position

```python
 | @property
 | estimated_position()
```

>**Returns**:

>The Monte Carlo Localization estimated position. None if there aren't one yet.

<a name=".mcl.MCL.prob_distribution"></a>
## prob\_distribution

```python
 | @prob_distribution.setter
 | prob_distribution(x)
```

Set the probability distribution with a copy of the new one.

>**Arguments**:

>- `x`: New probability distribution.

<a name=".mcl.MCL.map"></a>
## map

```python
 | @map.setter
 | map(x: Map)
```

Set the map with the new one (isn't a copy).
Also, updates the sensor and movement matrices.
Finally, set the real position to a random one.

>**Arguments**:

>- `x`: The new map.

<a name=".mcl.MCL.uniform_distribution"></a>
## uniform\_distribution

```python
 | @property
 | uniform_distribution() -> np.array
```

>**Returns**:

>The uniform probability distribution of the actual map

<a name=".mcl.MCL.reset_distribution"></a>
## reset\_distribution

```python
 | reset_distribution()
```

Reset the probability distribution to the uniform one. Automatically called when changing or setting the `map`.

Should be called after setting the `real_position` unless you want to analyse the kidnapped robot problem.

