
<a name=".simulator.Simulator"></a>
# Simulator

```python
class Simulator():
 |  Simulator(map_: Map)
```

The robot model simulator. It simulates the robot position, movements and sensor readings.

This implementation was not done to be general, is specific to our robot model. See more details in
[Virtual Chess Rook docs](https://the-monte-carlo-robots.readthedocs.io/projects/virtual-chess-rook/).

The noises are Simulator class attributes and are initialized with it default values:

 - `Simulator.p_hit = 0.9`: The probability of the measurement to be right.
 - `Simulator.p_miss = 0.1`: The probability of the measurement to be wrong.
 - `Simulator.p_exact = 0.8`: The probability of the robot perform an exact motion.
 - `Simulator.p_overshoot = 0.1`: The probability of the robot perform more than desired motion.
 - `Simulator.p_undershoot = 0.1`: The probability of the robot perform less than desired motion.

It also has class attributes to help dealing with movements commands. They are not Enum or other stuff to improve
code speed:

 - `left_cmd = 0`
 - `right_cmd = 1`
 - `up_cmd = 2`
 - `down_cmd = 3`

<a name=".simulator.Simulator.map"></a>
## map

```python
 | @map.setter
 | map(x: Map)
```

Set the map with the new one (isn't a copy).

>**Arguments**:

>- `x`: The new map.

<a name=".simulator.Simulator.simulate_movement"></a>
## simulate\_movement

```python
 | simulate_movement(cmd: int)
```

Do the simulation movement of the robot considering movement noises.
It updates the `Simulator.real_position` with the new place.

>**Arguments**:

>- `cmd`: The desired robot movement

<a name=".simulator.Simulator.simulate_sensor"></a>
## simulate\_sensor

```python
 | simulate_sensor() -> bool
```

Do the sensor measurement simulation considering sensor noises.
Number 1 or `True` represents landmark reading (black on the image representation).

>**Returns**:

>The measurement/sense.

<a name=".simulator.Simulator.real_position"></a>
## real\_position

```python
 | @real_position.setter
 | real_position(x)
```

Set the new robot real position. It accepts an absolute number meaning the sum of row and column coordinates.
Also accept a tuple in form (row, column). Or if you just want to set it to a random position set it to None.

Note: This is due to the simulation part

### Examples

>>> Simulator.real_position = (5, 5)
set to (5,5) coordinate in a 6x6 map

>>> Simulator.real_position = 35
set to (5,5) coordinate in a 6x6 map

>>> Simulator.real_position = None
set to a random place

>**Arguments**:

>- `x`: The robot real position coordinate.

