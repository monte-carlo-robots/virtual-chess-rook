
<a name=".simulated_annealing.SimulatedAnnealing"></a>
# SimulatedAnnealing

```python
class SimulatedAnnealing(object)
```

The implementation of the Simulated Annealing algorithm using the Boltzmann Distribution.

Note: This implementation is specific to a Map optimization.

### Attributes

- `temp`: The temperature in the Boltzmann Distribution.
- `__map`: The map object to be optimized.
When getting with `map` property it returns the `__map.copy()`.
Don't have a setter to it.
- `__best`: The best map found. Is initialize equal to the given map.
When getting with the `best` property it returns the `__best.copy()`.
Don't have a setter to it.
- `__best_value`: The `objective_function()` value for the best map found.
Has the `best_value` property to get it but don't have a setter.
- `__objective_function`: The objective function. Should never be accessed, see constructor for more details.

<a name=".simulated_annealing.SimulatedAnnealing.__init__"></a>
## \_\_init\_\_

```python
 | __init__(map_: Map, objective_function: Callable, temperature=0.01, minimize=False)
```

Create an instance of the SimulatedAnnealing object. This object finds a Map object that maximizes the
objective_function (or minimize it if `minimize=True`).

>**Arguments**:

>- `map_`: The Map to be optimized.
>- `objective_function`: The objective function to be maximized (or minimized if `minimize=True`). The
>objective_function should be somehow attached to the Map object and must have *NO* required arguments.
>- `temperature`: The Boltzmann Distribution temperature parameter, in this implementation is constant.
>- `minimize`: A flag indication if the objective function should be minimized, default is to maximize it.

<a name=".simulated_annealing.SimulatedAnnealing.optimize"></a>
## optimize

```python
 | optimize(transitions: int = 1000, draw: bool = False) -> (np.array, np.array)
```

Do the Simulated Annealing optimization maximizing the objective_function or minimizing it if constructed that
way.

>**Arguments**:

>- `transitions`: Number of Simulated Annealing transitions, a rejected one doesn't increment it counter.
>- `draw`: Flag indicating if should draw/save the map image for each transition.

>**Returns**:

>A tuple with two numpy arrays of size transitions containing, respectively, the best measure found and
>number of rejections for each transition.

